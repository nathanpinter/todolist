#pragma checksum "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8da2439f1427f5e36ffb8dbad70f98295033c637"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Todo_index), @"mvc.1.0.view", @"/Views/Todo/index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Todo/index.cshtml", typeof(AspNetCore.Views_Todo_index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\_ViewImports.cshtml"
using AspNetCoreTodo;

#line default
#line hidden
#line 2 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\_ViewImports.cshtml"
using AspNetCoreTodo.Models;

#line default
#line hidden
#line 2 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
using Humanizer;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8da2439f1427f5e36ffb8dbad70f98295033c637", @"/Views/Todo/index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"63823eaa5b73e495aebe7447edc96790f50c299d", @"/Views/_ViewImports.cshtml")]
    public class Views_Todo_index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TodoViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "MarkDone", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(40, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
  
    ViewData["Title"] = "Manage your todo list";

#line default
#line hidden
            BeginContext(99, 77, true);
            WriteLiteral("\r\n<div class=\"panel panel-default todo-panel\">\r\n  <div class=\"panel-heading\">");
            EndContext();
            BeginContext(177, 17, false);
#line 9 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
                        Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(194, 203, true);
            WriteLiteral("</div>\r\n\r\n  <table class=\"table table-hover\">\r\n      <thead>\r\n          <tr>\r\n              <td>&#x2714;</td>\r\n              <td>Item</td>\r\n              <td>Due</td>\r\n          </tr>\r\n      </thead>\r\n\r\n");
            EndContext();
#line 20 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
       foreach (var item in Model.Items)
      {

#line default
#line hidden
            BeginContext(448, 52, true);
            WriteLiteral("          <tr>\r\n              <td>\r\n                ");
            EndContext();
            BeginContext(500, 204, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "32d9fb3bf1854eb1be08c456b790a6bd", async() => {
                BeginContext(542, 119, true);
                WriteLiteral("\r\n                    <input type=\"checkbox\" class=\"done-checkbox\">\r\n                    <input type=\"hidden\" name=\"id\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 661, "\"", 677, 1);
#line 26 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
WriteAttributeValue("", 669, item.Id, 669, 8, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(678, 19, true);
                WriteLiteral(">\r\n                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(704, 41, true);
            WriteLiteral("\r\n              </td>\r\n              <td>");
            EndContext();
            BeginContext(746, 10, false);
#line 29 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
             Write(item.Title);

#line default
#line hidden
            EndContext();
            BeginContext(756, 25, true);
            WriteLiteral("</td>\r\n              <td>");
            EndContext();
            BeginContext(782, 21, false);
#line 30 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
             Write(item.DueAt.Humanize());

#line default
#line hidden
            EndContext();
            BeginContext(803, 24, true);
            WriteLiteral("</td>\r\n          </tr>\r\n");
            EndContext();
#line 32 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
      }

#line default
#line hidden
            BeginContext(836, 62, true);
            WriteLiteral("  </table>\r\n\r\n  <div class=\"panel-footer add-item-form\">\r\n    ");
            EndContext();
            BeginContext(899, 57, false);
#line 36 "C:\Users\nathanp\dotnet_training\AspNetCoreTodo\AspNetCoreTodo\Views\Todo\index.cshtml"
Write(await Html.PartialAsync("AddItemPartial", new TodoItem()));

#line default
#line hidden
            EndContext();
            BeginContext(956, 18, true);
            WriteLiteral("\r\n  </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TodoViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
